import time

from selenium import webdriver
from bs4 import BeautifulSoup as bSoup
import sqlite3

conn = sqlite3.connect('scrap.db')
c = conn.cursor()
c.execute("CREATE TABLE youtube(channel_id VARCHAR, video_name VARCHAR, channel_name VARCHAR, published_at VARCHAR)")

url = ['https://www.youtube.com/feed/explore'.format()]

def main():
    driver = webdriver.Chrome()
    driver.get(url[0])
    content = driver.page_source.encode('utf-8').strip()
    soup = bSoup(content, 'lxml')

    chid = [a['href'] for a in soup.find_all('a', class_='yt-simple-endpoint style-scope yt-formatted-string')[0::1]]
    channel_id = [i for n, i in enumerate(chid) if i in chid[:n]]
    chname = soup.findAll('a', class_='yt-simple-endpoint style-scope yt-formatted-string')[0::1]
    channel_name = [i for n, i in enumerate(chname) if i in chname[:n]]
    video_name = soup.findAll('a', id='video-title')[0::1]
    published_at = soup.findAll('span', class_='style-scope ytd-video-meta-block')[1::2]

    cd = sorted(channel_id)
    for cd in cd:
        c.execute('''INSERT INTO youtube(channel_id) VALUES(?)''', (cd,))

    cn = channel_name
    for cn in cn:
        cn = cn.text.strip()
        c.execute('''INSERT INTO youtube(channel_name) VALUES(?)''', (cn,))

    vn = video_name
    for vn in vn:
        vn = vn.text.strip()
        c.execute('''INSERT INTO youtube(video_name) VALUES(?)''', (vn,))

    pub = published_at
    for pub in pub:
        pub = pub.text.strip()
        c.execute('''INSERT INTO youtube(published_at) VALUES(?)''', (pub,))

    return
if __name__ == '__main__':
    while True:
        main()
        print(f'Waiting 2 seconds...')
        time.sleep(1)
        conn.commit()
        print('Database Berhasil Di Input')

c.execute('''SELECT * FROM youtube''')
hasil = c.fetchall()
print(len(hasil))

conn.close()
